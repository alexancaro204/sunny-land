extends Node2D

onready var itemIcon = $icon.texture;
onready var text = $text

export var itemName : String;

func _ready():
	return itemName || itemIcon

func _on_detect_body_entered(body):
	return body

func popup(time) -> void:
	text.show()
	
	$popup.interpolate_property(text, 
			"rect_position", 
			text.rect_position + Vector2(0, -10), 
			text.rect_position + Vector2(0, -30),
			time, 
			Tween.TRANS_BACK, 
			Tween.EASE_IN)
	
	$popup.start()
	yield($popup, "tween_completed")
	queue_free()

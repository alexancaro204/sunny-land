extends Control

func _input(event) -> void:
	if event.is_action_pressed("ui_accept"):
		get_tree().change_scene("res://Levels/SunnyLand.tscn")

func _on_press_pressed() -> void:
	get_tree().change_scene("res://Levels/SunnyLand.tscn")

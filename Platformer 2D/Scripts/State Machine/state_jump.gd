extends "res://Scripts/State Machine/state.gd"

func enter():
	owner.get_node("Anim").play("Jump")
	jump()

func jump():
	if owner.salto_actual < owner.saltos_disp:
		owner.velocity.y = -owner.jump_speed;
		owner.move_and_slide(owner.velocity,Vector2(0,-1));
		owner.salto_actual += 1

func handle_input(event):
	if Input.is_action_just_pressed("ui_up"):
		jump()

func update(delta):
	if owner.is_on_floor():
		emit_signal("finished","Idle");
		owner.salto_actual = 0
		owner.velocity.x = 0
		return;
	
	move(owner.speed, owner.direction);

func move(speed, direction) -> void:
	owner.velocity.x = direction.x * speed;
	owner.move_and_slide(owner.velocity, Vector2(0,-1));

extends Node

signal finished(next_state_name);

onready var anim_player: AnimationPlayer

func enter():
	emit_signal("finished");

func update(delta):
	return delta;

func handle_input(evemt):
	return evemt;

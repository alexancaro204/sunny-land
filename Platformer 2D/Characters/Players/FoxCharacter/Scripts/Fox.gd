extends KinematicBody2D

signal finished
signal camera_shake_requested

onready var state_machine: Node = $FoxStateMachine
onready var anim_player: AnimationPlayer = $Anim

export var speed: float = 45.50
export var gravity: float = 450

export var jump_speed: float = 130
export var saltos_disp: int = 2
var salto_actual: int = 0

var direction := Vector2()
var velocity := Vector2()
var distance := Vector2()

func _process(delta):
	pass

func _physics_process(delta):
	apply_gravity(true, delta)

func _input(event):
	direction.x = int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
	if direction.x:
		get_node("BodyPivot").scale = Vector2(direction.x, 1)
	return event

func apply_gravity(value, delta):
	if value:
		velocity.y += gravity*delta
		var move_info = move_and_slide(velocity, Vector2.UP)
		
		if get_slide_count() != 0:
			var body = get_slide_collision(get_slide_count()-1);
			
			if body.normal == Vector2.DOWN:
				velocity.y = 0
			
			if body.normal == Vector2.UP and body.collider.is_in_group("Enemies"):
				enemy_collision(body)
			
		if is_on_floor():
			velocity.y = 0
		
		return move_info

func enemy_collision(body) -> void:
	body.collider.dead()
	body.collider._apply_gravity(false, null)
	body.collider.set_physics_process(false)
	
	salto_actual -= 1 if salto_actual == 1 or salto_actual == 2 else 0
	
	emit_signal("finished", "Jump")
	emit_signal("camera_shake_requested", 4.0)

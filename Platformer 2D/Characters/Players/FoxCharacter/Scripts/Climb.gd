extends "res://Scripts/State Machine/state.gd"

func enter():
	owner.velocity = Vector2(0, 0)
	owner.get_node("Anim").play("Climb")
	owner.set_physics_process(false)

func handle_input(event: InputEvent):
	owner.direction.y = int(Input.is_action_pressed("ui_down"))-int(Input.is_action_pressed("ui_up"))
	
	if owner.direction.y != 0:
		owner.get_node("Anim").play("Climb")
	else:
		owner.get_node("Anim").stop()
	
	if event.is_action_released("ui_accept"):
		emit_signal("finished", "Idle")
		owner.set_physics_process(true)
		return

func update(delta):
	move(owner.speed, owner.direction, delta);

func move(speed, direction, delta):
	owner.velocity.y = direction.y * speed;
	owner.velocity.x = direction.x * speed;
	owner.move_and_slide(owner.velocity, Vector2(0,-1));

extends "res://Characters/Enemies/Enemy.gd"

func _ready():
	direction.x = -1

func _physics_process(delta):
	._apply_gravity(true,delta)
	
	if $raycast_left.is_colliding():
		if $raycast_left.get_collider().is_in_group("Players"):
			direction.x = -1
			$spr.flip_h = false
		else:
			direction.x = 1
			$spr.flip_h = true
	elif $raycast_right.is_colliding(): 
		if $raycast_right.get_collider().is_in_group("Players"):
			direction.x = 1
			$spr.flip_h = true
		else:
			direction.x = -1
			$spr.flip_h = false

func _on_anim_animation_finished(anim_name):
	if anim_name == "Died":
		queue_free()

func _on_DetectCollision_body_entered(body):
	if body.is_in_group("Players") and body.get_node("Anim").current_animation != "hurt":
		body.emit_signal("finished","Hurt");

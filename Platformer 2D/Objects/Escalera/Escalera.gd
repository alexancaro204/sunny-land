extends Node2D

var player: KinematicBody2D = null

func _ready() -> void:
	set_process(false)
	set_process_input(false)

func _input(event) -> void:
	if event.is_action_pressed("ui_up"):
		player.emit_signal("finished","Climb")

func _on_detect_area_entered(area: Area2D) -> void:
	if area.get_parent() && area.get_parent().is_in_group("Players"):
		player = area.get_parent()
		set_process_input(true)

func _on_detect_area_exited(area: Area2D) -> void:
	if area.get_parent() && area.get_parent().is_in_group("Players"):
		player.emit_signal("finished", "Idle")
		player.velocity = Vector2(0, 0)
		player.set_physics_process(true)
		set_process_input(false)
		player = null
